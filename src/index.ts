import "./style.css"
import 'vanilla-delegation'

const SELECTOR         = '.supt-customize-component-control'
const SELECTOR_HANDLE  = `${SELECTOR}__head`
const SELECTOR_CONTENT = `${SELECTOR}__content`
const SELECTOR_INNER   = `${SELECTOR}__inner`


interface HTMLFormElementAlt extends HTMLFormElement {
	addDelegateListener(eventType: string, selector: string, handler: Function, useCapture?: boolean): void
}
interface RefsType {
	handle: HTMLElement,
	content: HTMLElement,
	inner: HTMLFormElementAlt,
	input: HTMLInputElement,
	parent: HTMLElement,
}

interface StateType {
	isHidden: Boolean,
	value: Object,
}



class CustomizerComponentControl {
	el
	state: StateType
	refs: RefsType

	constructor(el: HTMLElement) {
		this.el = el

		this.refs = {
			handle: this.el.querySelector(SELECTOR_HANDLE),
			content: this.el.querySelector(SELECTOR_CONTENT),
			inner: this.el.querySelector(SELECTOR_INNER),
			input: this.el.querySelector('[data-customize-setting-link]'),
			parent: this.el.closest('.accordion-section'),
		}

		let value;
		try { value = JSON.parse(this.refs.input.value) }
		catch { value = {} }

		this.state = {
			isHidden: true,
			value,
		}

		this.refs.inner.addDelegateListener('change', 'input,select,textarea', this.onSubInputChange.bind(this))

		if ( this.refs.handle ) {
			this.refs.handle.addEventListener('click', this.onHandleClick.bind(this) )
			this.refs.parent.addEventListener('component-open', this.onComponentOpen.bind(this))

			this.setContentHeight()
		}

	}

	onHandleClick(event: Event) {
		event.preventDefault()

		this.state.isHidden = !this.state.isHidden
		this.updateDOM()

		if ( !this.state.isHidden )
			this.refs.parent.dispatchEvent(new CustomEvent('component-open', { detail: { id: this.el.id }}))
	}

	onSubInputChange(event: Event) {

		this.refs.input.value = this.getFormInputJSON(this.refs.inner);
		this.refs.input.dispatchEvent(new Event('change'));
	}

	onComponentOpen(event: CustomEvent) {
		if ( event.detail.id !== this.el.id ) {
			this.state.isHidden = true
			this.updateDOM();
		}
	}

	updateDOM() {
		this.el.classList.toggle( 'is-open', !this.state.isHidden )
		this.refs.content.setAttribute( 'aria-hidden', this.state.isHidden.toString() )
	}

	setContentHeight() {
		this.refs.content.style.height = `${this.refs.inner.offsetHeight}px`
	}

	getFormInputJSON(form: HTMLFormElement): string {
		const data: FormData = new FormData(form);
		var object: any = {};
		data.forEach((value, key) => {

			const matches: Array<string>|null = key.match(/([^[]+)(?:\[([^\]]*)\])?/i)
			if ( matches !== null ) {
				const subObj = ( Reflect.has(object, matches[1])
					? {...object[matches[1]], [matches[2]]: value}
					: { [matches[2]]: value}
				)
				object[matches[1]] = subObj
			}
			else {
				object[matches[0]] = value
			}
		});

		return JSON.stringify(object);
	}

	destroy() {

	}
}

let controls: Array<CustomizerComponentControl>

// @ts-ignore
wp.customize.bind('ready', function () {
	const elements = Array.from(document.querySelectorAll(SELECTOR))
	controls = elements.map((el: HTMLElement) => new CustomizerComponentControl(el) )
});

