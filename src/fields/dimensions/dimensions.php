<?php

namespace SUPT\Customizer\Control\Fields\Dimensions;

function render_field($id, $name, $attrs, $echo = true) {
	$sub_fields = [];

	foreach ($attrs['dimensions'] as $sub_name => $sub_attrs) {
		if ( !empty($sub_attrs['unit']) || !empty($attrs['unit']) ) $units = $sub_attrs['unit'] ?? $attrs['unit'];
		$sub_data = [
			'%field_id'    => "{$id}_{$name}_{$sub_name}_field",
			'%id'          => "{$id}_{$name}_{$sub_name}",
			'%label'       => $sub_attrs['label'] ?? $sub_name,
			'%type'        => $sub_attrs['type'] ?? 'text',
			'%name'        => "{$name}[$sub_name]",
			'%unit'        => ( empty($units) ? '' : " data-unit=\"$units\"" ),
			'%placeholder' => $sub_attrs['placeholder'] ?? '',
			'%value'       => $attrs['value'][$sub_name] ?? $sub_attrs['default'] ?? $attrs['default'] ?? '',
			'%attrs'       => (empty($sub_attrs['attrs']) ? '' :	' '.implode( ' ',array_map(
				function($k) use ($sub_attrs) {
					return "{$k}=\"{$sub_attrs['attrs'][$k]}\"";
				},
				array_keys($sub_attrs['attrs'])
			))),
		];
		$sub_fields[] = str_replace(
			array_keys($sub_data),
			array_values($sub_data),
			'<div class="dimensions-field__item" id="%field_id"%unit>
				<label for="%id">%label</label>
				<input type="%type" id="%id" name="%name" value="%value" placeholder="%placeholder"%attrs />
			</div>'
		);
	}

	$data = [
		'%label'      => $attrs['label'],
		'%name'       => $attrs['name'],
		'%sub_fields' => implode("\n", $sub_fields),
	];

	$html = str_replace(
		array_keys($data),
		array_values($data),
		'<fieldset class="supt-customize-component-control__field dimensions-field">
			<legend class="dimensions-field__legend">%label</legend>
			<div class="dimensions-field__inner">
				%sub_fields
			</div>
		</fieldset>'
	);

	if ($echo) echo $html;
	return $html;
}
