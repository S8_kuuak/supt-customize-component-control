<?php

namespace SUPT\Customizer\Control\Fields\Typography;

class Walker {

	function __construct($id, $name, $attrs) {
		$this->id = $id;
		$this->name = $name;
		$this->attrs = $attrs;
	}

	function get_id() {
		return "{$this->id}_{$this->name}";
	}

	function get_rendered() {
		return
			'<fieldset class="supt-customize-component-control__field field-typography">
				<legend class="field-typography__legend">'. $this->attrs['label'] .'</legend>
				<div class="field-typography__inner">
					'. $this->get_family_field() .'
					'. $this->get_weight_field() .'
					'. $this->get_size_fields() .'
					'. $this->get_alignment_field() .'
					'. $this->get_transform_field() .'
					'. $this->get_color_field() .'
				</div>
			</fieldset>';
	}

	function get_family_field() {
		if ( !in_array('family', $this->attrs['supports']) ) return '';

		return $this->get_rendered_select(
			"{$this->get_id()}_fontFamily",
			"{$this->name}[fontFamily]",
			'--font-family',
			__('Font Family', 'supt-ccc'),
			[
				'primary'   => __( 'Primary', 'supt-ccc'),
				'secondary' => __( 'Secondary', 'supt-ccc'),
			],
			$this->attrs['value']['fontFamily'] ?? $this->attrs['default']['fontFamily'] ?? '',
		);
	}

	function get_weight_field() {
		if ( !in_array('weight', $this->attrs['supports']) ) return '';

		$weights = [
			'100' => __('Thin (Hairline)', 'supt-ccc'),
			'200' => __('Extra Light (Ultra Light)', 'supt-ccc'),
			'300' => __('Light', 'supt-ccc'),
			'400' => __('Normal (Regular)', 'supt-ccc'),
			'500' => __('Medium', 'supt-ccc'),
			'600' => __('Semi Bold (Demi Bold)', 'supt-ccc'),
			'700' => __('Bold', 'supt-ccc'),
			'800' => __('Extra Bold (Ultra Bold)', 'supt-ccc'),
			'900' => __('Black (Heavy)', 'supt-ccc'),
		];
		// TODO find how to only show weight available
		// 	$available = get_available_weights($this->name);
		// 	$weights = array_intersect_key($weights, $variants);

		return $this->get_rendered_select(
			"{$this->get_id()}__fontWeight",
			"{$this->name}[fontWeight]",
			'--font-weight',
			__('Font Weight', 'supt-ccc'),
			$weights,
			$this->attrs['value']['fontWeight'] ?? $this->attrs['default']['fontWeight'] ?? '',
		);
	}

	function get_size_fields() {
		if ( !in_array('size', $this->attrs['supports']) ) return '';

		$data = [
			'%id'            => $this->get_id(),
			'%name'          => $this->name,

			'%label_sizemin'       => __('Min font size', 'supt-ccc'),
			'%label_sizemax'       => __('Max font size', 'supt-ccc'),
			'%label_lineheight'    => __('Line Height', 'supt-ccc'),
			'%label_letterspacing' => __('Letter Spacing', 'supt-ccc'),

			'%fontSizeMin'   => $this->attrs['value']['fontSize']['min'] ?? $this->attrs['default']['fontSize']['min'] ?? '',
			'%fontSizeMax'   => $this->attrs['value']['fontSize']['max'] ?? $this->attrs['default']['fontSize']['max'] ?? '',
			'%lineHeight'    => $this->attrs['value']['lineHeight']      ?? $this->attrs['default']['lineHeight']      ?? '',
			'%letterSpacing' => $this->attrs['value']['letterSpacing']   ?? $this->attrs['default']['letterSpacing']   ?? '',
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="field-typography__item --font-size-min">
				<label for="%id_fontSize_min">%label_sizemin</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_fontSize_min" name="%name[fontSize][min]" value="%fontSizeMin" />
				</div>
			</div>
			<div class="field-typography__item --font-size-max">
				<label for="%id_fontSize_max">%label_sizemax</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_fontSize_max" name="%name[fontSize][max]" value="%fontSizeMax" />
				</div>
			</div>
			<div class="field-typography__item --line-height">
				<label for="%id_lineHeight">%label_lineheight</label>
				<div class="field-typography__item-row">
					<span>em</span>
					<input type="number" id="%id_lineHeight" name="%name[lineHeight]" value="%lineHeight" />
				</div>
			</div>
			<div class="field-typography__item --letter-spacing">
				<label for="%id_letterSpacing">%label_letterspacing</label>
				<div class="field-typography__item-row">
					<span>em</span>
					<input type="number" id="%id_letterSpacing" name="%name[letterSpacing]" value="%letterSpacing" />
				</div>
			</div>',
		);
	}

	function get_alignment_field() {
		if ( !in_array('align', $this->attrs['supports']) ) return '';

		$items = [
			['%id' => "{$this->get_id()}_textAlign_reset",   '%label' => __('Clear', 'supt-ccc'),   '%name' => "{$this->name}[textAlign]", "%value" => '',        '%icon' => 'removeformatting' ],
			['%id' => "{$this->get_id()}_textAlign_left",    '%label' => __('Left', 'supt-ccc'),    '%name' => "{$this->name}[textAlign]", "%value" => 'left',    '%icon' => 'alignleft' ],
			['%id' => "{$this->get_id()}_textAlign_center",  '%label' => __('Center', 'supt-ccc'),  '%name' => "{$this->name}[textAlign]", "%value" => 'center',  '%icon' => 'aligncenter' ],
			['%id' => "{$this->get_id()}_textAlign_right",   '%label' => __('Right', 'supt-ccc'),   '%name' => "{$this->name}[textAlign]", "%value" => 'right',   '%icon' => 'alignright' ],
			['%id' => "{$this->get_id()}_textAlign_justify", '%label' => __('Justify', 'supt-ccc'), '%name' => "{$this->name}[textAlign]", "%value" => 'justify', '%icon' => 'justify' ],
		];

		$value = $this->attrs['value']['textAlign'] ?? $this->attrs['default']['textAlign'] ?? null;

		$rendered_items = array_map(function($item) use ($value) {
			$item['%checked'] = ( (!empty($value) && $item['%value'] == $value) ? ' checked' : '' );
			return str_replace(
				array_keys($item),
				array_values($item),
				'<div>
					<input type="radio" id="%id" name="%name" value="%value"%checked />
					<label for="%id" class="dashicons dashicons-editor-%icon">%label</label>
				</div>'
			);
		}, $items);

		return sprintf(
			'<div class="field-typography__item --text-align">
				<span>Text Align</span>
				%s
			</div>',
			implode("\n", $rendered_items)
		);
	}

	function get_transform_field() {
		if ( !in_array('transform', $this->attrs['supports']) ) return '';

		return $this->get_rendered_select(
			"{$this->get_id()}_textTransform",
			"{$this->name}[textTransform]",
			'--text-transform',
			__('Text Transform', 'supt-ccc'),
			[
				''               => __('None', 'supt-ccc'),
				'capitalize'     => __('Capitalize', 'supt-ccc'),
				'uppercase'      => __('Uppercase', 'supt-ccc'),
				'lowercase'      => __('Lowercase', 'supt-ccc'),
				'full-width'     => __('Full-width', 'supt-ccc'),
				'full-size-kana' => __('Full-size-kana', 'supt-ccc'),
			],
			$this->attrs['value']['textTransform'] ?? $this->attrs['default']['textTransform'] ?? '',
		);
	}

	function get_color_field() {
		if ( !in_array('color', $this->attrs['supports']) ) return '';

		return sprintf(
			'<div class="field-typography__item --color">
				<label for="%1$s">%3$s</label>
				<input type="color" id="%id_color" name="%2$s" value="%4$s" />
			</div>',
			"{$this->id}_color",
			"{$this->name}[color]",
			__( 'Color', 'supt-ccc' ),
			$this->attrs['value']['color'] ?? $this->attrs['default']['color'] ?? '#000000',
		);
	}


	function get_rendered_select($id, $name, $cls, $label, $choices = [], $value = null) {

		$opts = [];
		foreach ($choices as $optValue => $optLabel) {
			$opts[] = sprintf(
				'<option value="%1$s"%3$s>%2$s</option>',
				$optValue,
				$optLabel,
				( $value == $optValue ? ' selected' : '')
			);
		}

		$select_data = [
			'%id'      => $id,
			'%name'    => $name,
			'%class'   => $cls,
			'%label'   => $label,
			'%options' => implode("\n", $opts),
		];


		return str_replace(
			array_keys($select_data),
			array_values($select_data),
			'<div class="field-typography__item %class">
				<label for="%id">%label</label>
				<select id="%id" name="%name">
					%options
				</select>
			</div>'
		);
	}
}

function render_field($id, $name, $attrs, $echo = true) {
	$walker = new Walker($id, $name, $attrs);
	$html = $walker->get_rendered();

	if ($echo) echo $html;
	return $html;
}


