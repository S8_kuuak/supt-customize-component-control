<?php
/**
 * Plugin Name: Customize Component Control
 * Plugin URI:  https://gitlab.com/S8_kuuak/supt-customize-component-control
 * Description: WordPress Customizer's custom control to configure component options.
 * Author:      Felipe (@kuuak)
 * Author URI:  https://profiles.wordpress.org/kuuak/
 * Version:     1.0.0
 * License:     GNU General Public License v3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace SUPT\Customizer\Control;

use WP_Customize_Control;

use function SUPT\Customizer\Control\Fields\Dimensions\render_field as render_field_dimensions;
use function SUPT\Customizer\Control\Fields\Typography\render_field as render_field_typography;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );
define( "SUPT_COMP_CONTROL_PLUGIN_VERSION", "1.0.0" );

require_once __DIR__.'/src/fields/_loader.php';

/**
 * Action & filter hooks
 */
add_action('init', __NAMESPACE__.'\register_control' );

function register_control() {

	if ( !class_exists('WP_Customize_Control') ) return;
	class ComponentControl extends WP_Customize_Control {
		public $type = 'component';
		public $is_multi = false;

		/**
		 * Sub fields of the Control
		 * @var array
		 */
		public $fields = [];

		public function enqueue() {
			wp_enqueue_style( 'supt_customize_component_control', plugins_url('dist/supt-customize-component-control.css', __FILE__), null, SUPT_COMP_CONTROL_PLUGIN_VERSION );
			wp_enqueue_script( 'supt_customize_component_control', plugins_url('dist/supt-customize-component-control.js', __FILE__), null,  SUPT_COMP_CONTROL_PLUGIN_VERSION, true);
		}

		public function render_content() {
			$value = $this->value();

			$data = [
				'%id'     => esc_attr( $this->id ),
				'%label'  => esc_html( $this->label ),
				'%desc'   => wp_kses_post( $this->description ),
				'%fields' => $this->get_rendered_fields(),
				'%value'  => ( empty($value) ? '{}' : esc_attr($value) ),
				'%link'   => $this->get_link(),
			];

			$template = ( $this->is_multi
				? '<div class="supt-customize-component-control" id="supt-customize-component-control-%id" >
						<button class="supt-customize-component-control__head">
							<span class="supt-customize-component-control__head-title">%label</span>
							<i class="dashicons dashicons-arrow-down-alt2"></i>
						</button>
						<div class="supt-customize-component-control__content" aria-hidden="true">
							<form class="supt-customize-component-control__inner">
								<p class="supt-customize-component-control__desc">%desc</p>
								<div class="supt-customize-component-control__fields">%fields</div>
							</div>
						</form>
						<input type="hidden" name="%id" value="%value" %link />
					</div>'
				: '<div class="supt-customize-component-control" id="supt-customize-component-control-%id" >
						<form class="supt-customize-component-control__inner">
							<div class="supt-customize-component-control__fields">%fields</div>
						</form>
						<input type="hidden" name="%id" value="%value" %link />
					</div>'
			);

			echo str_replace( array_keys($data), array_values($data), $template );
		}

		private function get_rendered_fields() {
			$html = [];
			$value = json_decode($this->value(), true);
			foreach ($this->fields as $name => $attrs) {

				$attrs['value'] = $value[$name] ?? null;

				switch ($attrs['type']) {
					case 'dimensions':
						$html[] = render_field_dimensions($this->id, $name, $attrs, false);
						break;

					case 'typography':
						$html[] = render_field_typography($this->id, $name, $attrs, false);
						break;

					default:
						$html[] = sprintf(
							'<div class="supt-customize-component-control__field ">
								<label for="%1$s">%2$s</label>
								<input type="%3$s" id="%1$s" name="%4s$" value="%5$s"/>
							</div>',
							"{$this->id}_$name",
							$attrs['label'],
							$attrs['type'],
							$name,
							$this->value()
						);
						break;
				}
			}

			return implode("\n", $html);
		}
	}

}

